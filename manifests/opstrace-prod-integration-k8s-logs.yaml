apiVersion: v1
kind: Namespace
metadata:
  name: opstrace
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: opstrace-promtail
  namespace: opstrace
data:
  promtail.yml: |
    clients:
    - url: https://loki.prod.k8s.nagyv.com/loki/api/v1/push
      bearer_token_file: /var/run/tenant-auth/token

    positions:
      # Must be writable by promtail, and should persist across restarts
      filename: /positions/positions.yaml

    scrape_configs:
    # Collection of per-pod logs
    - job_name: kubernetes-pods
      kubernetes_sd_configs:
      - role: pod

      # Specify whether the data is cri (tsv) or dockerd (json) format
      pipeline_stages:
      - cri:

      relabel_configs:
      # Include <namespace>/<pod name>
      - action: replace
        source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_pod_name]
        separator: /
        replacement: $1
        target_label: job
      # Include namespace
      - action: replace
        source_labels: [__meta_kubernetes_namespace]
        target_label: namespace
      # Include pod name without namespace
      - action: replace
        source_labels: [__meta_kubernetes_pod_name]
        target_label: instance
      # Include parent replicaset/statefulset/daemonset name
      - action: replace
        source_labels: [__meta_kubernetes_pod_controller_name]
        target_label: controller
      # Include container name within pod
      - action: replace
        source_labels: [__meta_kubernetes_pod_container_name]
        target_label: container
      # Include node name
      - action: replace
        source_labels: [__meta_kubernetes_pod_node_name]
        target_label: node

      # Include integration ID for separation in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: e81599ef-13cc-4025-b6d8-1e239ab1beab

      # Internal labels used by promtail itself
      # Map node hostname to __host__, used by promtail to decide which instance should scrape the pod
      - source_labels: [__meta_kubernetes_pod_node_name]
        target_label: __host__
      # Map container to expected path: /var/log/pods/<namespace>_<podname>_<uuid>/<containername>/*.log
      - replacement: /var/log/pods/*$1/*.log
        separator: /
        source_labels: [__meta_kubernetes_pod_uid, __meta_kubernetes_pod_container_name]
        target_label: __path__
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: opstrace-promtail
  namespace: opstrace
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: opstrace-promtail
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: opstrace-promtail
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: opstrace-promtail
subjects:
- kind: ServiceAccount
  name: opstrace-promtail
  namespace: opstrace
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: opstrace-promtail
  namespace: opstrace
spec:
  minReadySeconds: 10
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      name: opstrace-promtail
  template:
    metadata:
      labels:
        name: opstrace-promtail
    spec:
      serviceAccount: opstrace-promtail
      tolerations:
      - effect: NoSchedule
        operator: Exists
      containers:
      - name: promtail
        image: grafana/promtail:2.2.1
        imagePullPolicy: IfNotPresent
        args:
        - -config.file=/etc/promtail/promtail.yml
        env:
        # Used by promtail to detect which pods are on the same node
        - name: HOSTNAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        ports:
        - name: ui
          containerPort: 80
        readinessProbe:
          httpGet:
            path: /ready
            port: ui
            scheme: HTTP
          initialDelaySeconds: 10
        securityContext:
          # Pod logs are only accessible to uid=root
          runAsUser: 0
        volumeMounts:
        # Promtail configmap
        - name: config
          mountPath: /etc/promtail
        # Opstrace tenant auth secret
        - name: tenant-auth
          mountPath: /var/run/tenant-auth
          readOnly: true
        # Read-only access to /var/log/pods/*
        - name: varlogpods
          mountPath: /var/log/pods
          readOnly: true
        # Read-write access to /var/log/opstrace-promtail for positions file
        - name: varlogopstracepromtail
          mountPath: /positions
          readOnly: false
      volumes:
      - name: config
        configMap:
          name: opstrace-promtail
      - name: tenant-auth
        secret:
          secretName: opstrace-tenant-auth
      - name: varlogpods
        hostPath:
          path: /var/log/pods
      - name: varlogopstracepromtail
        hostPath:
          path: /var/log/opstrace-promtail
